use std::str::FromStr;

use gpgme;
use lettre::{self, Transport};

fn main() {
    // add your data here
    let username = "TODO".to_string();
    let password = "TODO".to_string();
    let host = "example.com".to_string();
    let key_fingerprint = "2F41D7EE4EC511199A5468D645ED94248268D858";
    let recipient_email = "foo@example.com".to_string();
    let subject = "Test";
    let message_text: Vec<u8> = Vec::from("Hello World!".to_string());

    // prepare mailer
    let credentials = lettre::transport::smtp::authentication::Credentials::new(
        username.clone(),
        password.clone(),
    );
    let mailer = {
        lettre::SmtpTransport::relay(&host)
            .unwrap()
            .credentials(credentials)
            .tls(lettre::transport::smtp::client::Tls::Required(
                lettre::transport::smtp::client::TlsParameters::new(host.clone()).unwrap(),
            ))
            .port(25)
            .build()
    };
    mailer.test_connection().unwrap();

    // prepare message
    // Note: use a `lettre::message::SinglePart` or a `lettere::message::MultiPart` here. They both work.
    let single_part = lettre::message::SinglePartBuilder::new()
        .content_type(lettre::message::header::ContentType::TEXT_PLAIN)
        .header(lettre::message::header::ContentTransferEncoding::QuotedPrintable)
        .body(message_text.clone());

    // create signature
    let mut ctx = gpgme::Context::from_protocol(gpgme::Protocol::OpenPgp).unwrap();
    ctx.set_armor(true);
    let key = ctx.get_secret_key(key_fingerprint).unwrap();
    ctx.add_signer(&key).unwrap();
    let mut sign_output: Vec<u8> = Vec::new();
    let mut sign_input: Vec<u8> = Vec::from(single_part.formatted().trim_ascii_end()); // use exactly this as signing input
    println!(
        "sign_input: >{}<",
        String::from_utf8(sign_input.clone()).unwrap()
    );
    let sign_result = ctx
        .sign(gpgme::SignMode::Detached, &mut sign_input, &mut sign_output)
        .unwrap();
    let hash_alg = sign_result
        .new_signatures()
        .next()
        .unwrap()
        .hash_algorithm();
    println!(
        "signature: >{}<",
        String::from_utf8(sign_output.clone()).unwrap()
    );

    // finalize message
    let message = lettre::message::MessageBuilder::new()
        .from(lettre::message::Mailbox::new(
            None,
            lettre::Address::new(username, host).unwrap(),
        ))
        .to(lettre::message::Mailbox::new(
            None,
            lettre::Address::from_str(&recipient_email).unwrap(),
        ))
        .subject(subject)
        .multipart(
            lettre::message::MultiPart::signed(
                "application/pgp-signature".to_string(),
                format!("pgp-{}", hash_alg.to_string().to_lowercase()),
            )
            .singlepart(single_part)
            .singlepart(
                lettre::message::Attachment::new("signature.asc".to_string()).body(
                    sign_output,
                    lettre::message::header::ContentType::from_str("application/pgp-signature")
                        .unwrap(),
                ),
            ),
        )
        .unwrap();

    // send
    mailer.send(&message).unwrap();
    println!("successfully sent message");
}
