#  send PGP signed messages in Rust

This is a MWE to send a PGP-signed messages from Rust, also see [lettre#963](https://github.com/lettre/lettre/issues/963).

## Prerequisites
```sh
sudo apt install libgpgme-error-dev libgpgme-dev
```

## Run
- add your credentials, etc. at the top of [`src/main.rs`](src/main.rs)
- run with `cargo run`